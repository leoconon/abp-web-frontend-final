# Front-end projeto ABP

### Nome do projeto
Não foi decidido um nome para o sistema. Ele fará parte do projeto **Recicla Rincão**, mas o nome da ferramenta deverá conter algo referente a coleta seletiva e comunidade.

### Integrantes
* HIGOR DOMINGOS MACHADO
* JOÃO PAULO PACHECO DA SILVA
* LEONARDO DA SILVA NONNENMACHER
* WESLEY GUSTAVO DE FAVERI CRAVO

### Objetivo
* Organizar e facilitar acesso à informação referente à coleta de lixo;
* Exigir transparência dos órgãos públicos quanto às informações referentes ao destinio dos resíduos;
* Estimular a coleta seletiva através de um sistema de pontuação, com retribuições por separação e descartes corretos de lixo;
* Viabilizar o projeto **Recicla Rincão**.

### Público-alvo
Prefeituras, empresas de coleta e sociedade em geral.

### Tecnologias
* Node.js
* React

#### Relacionados:
* Link código fonte do server: https://gitlab.com/wesleyfaveri/abp-web
* Link da página: https://abp-web.herokuapp.com/ 
