import React, { Component } from 'react';
import { InputGroup, Icon, Input, IconButton, SelectPicker, Table, Modal, Button, Radio, FormGroup, RadioGroup, DatePicker, InputNumber } from 'rsuite';
const { Column, HeaderCell, Cell } = Table;

export default class UserArea extends Component {
  constructor() {
    super();
    this.state = {
      logando: false,
      usuarioDeslogado: {},
    };
  }

  componentWillMount() {
    this.setState({ usuario: window.usuario });
  }

  logar() {
    if (this.state.logando) {
      return;
    }
    this.setState({ logando: true });
    fetch('/api/login', {
      method: 'POST',
      body: JSON.stringify(this.state.usuarioDeslogado),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      return response.json();
    }).then(usr => {      
      console.log(usr);
      if (usr.nome === undefined) {
        alert('Usuário ou senha incorreta');
        this.setState({ logando: false });
        return;
      }
      window.usuario = usr;
      this.setState({ usuario: usr, logando: false });
    }).catch(err => {
      alert('Usuário ou senha incorreta');
      this.setState({ logando: false });
      console.log(err);
    });
  }

  handlerNomeUsuario(login) {
    this.state.usuarioDeslogado.login = login;
  }

  handlerSenha(senha) {
    this.state.usuarioDeslogado.senha = senha
  }

  componentDidMount() {
    fetch('/api/estados')
      .then(response => {
        return response.json();
      })
      .then(list => {
        let values = list.map(e => ({ label: e.nome, value: e }));
        this.setState({ estados: values });
      });
  }

  handlerEstado(value) {
    fetch('/api/cidades')
      .then(response => {
        return response.json();
      })
      .then(list => {
        let values = list.filter(c => c.id_estado === value.id).map(c => ({ label: c.nome, value: c }));
        this.setState({ cidades: values });
      });
  }

  handlerCidade(value) {
    fetch(`/api/locais/${value.id_estado}/${value.id}`)
      .then(response => {
        return response.json();
      })
      .then(list => {
        let values = list.map(b => ({ label: b.nome, value: b }));
        this.setState({ bairros: values });
      });
  }

  handlerBairro(value) {
    this.setState({ bairroSelecionado: value });
    fetch(`/api/rotas/bairro/${value.id}`)
      .then(response => {
        return response.json();
      })
      .then(list => {
        list.map(l => {
          l.periodo_nome = this.nomePeriodo(l.periodo);
          l.dia_semana_nome = this.nomeDiaSemana(l.dia_semana);
          return l;
        })
        this.setState({ rotas: list });
      });
  }

  nomeDiaSemana(num) {
    switch (num) {
      case 1: return 'Sábado';
      case 2: return 'Segunda';
      case 3: return 'Terça';
      case 4: return 'Quarta';
      case 5: return 'Quinta';
      case 6: return 'Sexta';
      default: return 'Erro';
    }
  }

  nomePeriodo(letra) {
    if (letra === 'V') {
      return 'Vespertino';
    }
    return 'Matutino';
  }

  abrirModal(rota) {
    this.refs.modal.open(rota);
  }

  abrirModalScore(rota) {
    this.refs.modalScore.open(rota);
  }

  refreshTable() {
    fetch(`/api/rotas/bairro/${this.state.bairroSelecionado.id}`)
      .then(response => {
        return response.json();
      })
      .then(list => {
        list.map(l => {
          l.periodo_nome = this.nomePeriodo(l.periodo);
          l.dia_semana_nome = this.nomeDiaSemana(l.dia_semana);
          return l;
        })
        this.setState({ rotas: list });
      });
  }

  removerBairro(rota) {
    fetch('/api/rota/' + rota.id, {
      method: 'DELETE',
    }).then(res => {
      this.refreshTable();
    }).catch(err => { alert('Não foi possível salvar'); console.log(err); });
  }

  render() {
    const styleInput = { maxWidth: '300px', marginBottom: '10px' };

    if (this.state.usuario === undefined) return (
      <div>
        <h2> Faça o login para cadastrar informações </h2>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <InputGroup style={styleInput} inside>
            <InputGroup.Addon>
              <Icon icon="avatar" />
            </InputGroup.Addon>
            <Input placeholder="Usuário" onChange={this.handlerNomeUsuario.bind(this)} />
          </InputGroup>
          <InputGroup style={styleInput} inside>
            <InputGroup.Addon>
              <Icon icon="key" />
            </InputGroup.Addon>
            <Input placeholder="Senha" type="password" onChange={this.handlerSenha.bind(this)} />
          </InputGroup>
          <div className="more">
            <IconButton icon={<Icon icon="sign-in" />} color="green" onClick={this.logar.bind(this)} loading={this.state.logando}>Login</IconButton>
          </div>
        </div>
      </div>
    );

    let selectEstado, selectCidade, selectBairro;

    if (this.state.estados !== undefined) {
      selectEstado = <SelectPicker style={styleInput} data={this.state.estados} placeholder="Estado" onChange={this.handlerEstado.bind(this)} />;
    }

    if (this.state.cidades !== undefined) {
      selectCidade = <SelectPicker style={styleInput} data={this.state.cidades} placeholder="Cidade" onChange={this.handlerCidade.bind(this)} />;
    }

    if (this.state.bairros !== undefined) {
      selectBairro = <SelectPicker style={styleInput} data={this.state.bairros} placeholder="Bairro" onChange={this.handlerBairro.bind(this)} />;
    }

    return (
      <div>
        <div style={{ padding: '20px 20px 0px 20px' }}>
          <div style={{ fontSize: '30px', fontWeight: 'bold' }}> Olá {window.usuario.nome}, </div>
          <div style={{ marginBottom: '10px', paddingLeft: '2px' }}> Aqui estão as informações disponíveis pra você </div>
        </div>
        <div>
          <h2> Selecione sua Região </h2>
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            {selectEstado}
            {selectCidade}
            {selectBairro}
          </div>
        </div>
        <div style={{ padding: '20px' }}>
          <div> Horários de coleta </div>
          <Table data={this.state.rotas} autoHeight renderEmpty={() => <div> Sem Informações </div>}>
            <Column align="center" fixed>
              <HeaderCell>Dia da semana</HeaderCell>
              <Cell dataKey="dia_semana_nome" />
            </Column>
            <Column align="center" fixed>
              <HeaderCell>Período</HeaderCell>
              <Cell dataKey="periodo_nome" />
            </Column>
            <Column width={250}>
              <HeaderCell>Ações</HeaderCell>
              <Cell>
                {rowData => {
                  return (
                    <span>
                      <a onClick={() => { this.abrirModal(rowData) }}> Editar </a> |{' '}
                      <a onClick={() => { this.abrirModalScore(rowData) }}> Nova Coleta </a> |{' '}
                      <a onClick={() => { this.removerBairro(rowData) }}> Remover </a>
                    </span>
                  );
                }}
              </Cell>
            </Column>
          </Table>
          <IconButton onClick={() => { this.abrirModal({ id_bairro: this.state.bairroSelecionado.id }) }} style={{ float: 'right' }} icon={<Icon icon="plus-circle" />} />
        </div>
        <RotesEdit ref="modal" refreshTable={this.refreshTable.bind(this)} />
        <NewScore ref="modalScore" refreshTable={this.refreshTable.bind(this)} />
      </div>
    );
  }
}

class RotesEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      loading: false,
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }
  close() {
    this.setState({ show: false });
  }
  open(rota) {
    this.setState({ show: true, rota: rota });
  }
  changeDayHandler(num) {
    this.state.rota.dia_semana = num;
    this.setState({ rota: this.state.rota });
  }
  changePerHandler(p) {
    this.state.rota.periodo = p;
    this.setState({ rota: this.state.rota });
  }
  salvarRota() {
    this.setState({ loading: true });
    let rota = this.state.rota;
    rota.diaSemana = rota.dia_semana;
    rota.idBairro = rota.id_bairro;
    fetch('/api/rota', {
      method: this.state.rota.dia_semana_nome === undefined ? 'POST' : 'PUT',
      body: JSON.stringify(rota),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      this.props.refreshTable();
      this.close();
      this.setState({ loading: false });
    }).catch(err => { alert('Não foi possível salvar'); this.setState({ loading: true }); });
  }
  render() {
    if (this.state.rota === undefined) {
      return <div></div>
    }
    return (
      <Modal show={this.state.show} onHide={this.close} style={{ marginTop: '100px' }}>
        <Modal.Header>
          <Modal.Title> Editar Rota </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormGroup controlId="radioList">
            <p>Dia da semana</p>
            <RadioGroup name="radioListDia" inline value={this.state.rota.dia_semana} onChange={value => { this.changeDayHandler(value); }}>
              <Radio value={2}> Segunda </Radio>
              <Radio value={3}> Terça </Radio>
              <Radio value={4}> Quarta </Radio>
              <Radio value={5}> Quinta </Radio>
              <Radio value={6}> Sexta </Radio>
              <Radio value={1}> Sábado </Radio>
            </RadioGroup>
            <p>Período</p>
            <RadioGroup name="radioListPeriodo" inline value={this.state.rota.periodo} onChange={value => { this.changePerHandler(value); }}>
              <Radio value="M"> Matutino </Radio>
              <Radio value="V"> Vespertino </Radio>
            </RadioGroup>
            <p>Metas</p>
            <DatePicker format="MM/YYYY" ranges={[]} />
            <InputNumber />
          </FormGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.salvarRota.bind(this)} appearance="primary" loading={this.state.loading}>
            Salvar
            </Button>
          <Button onClick={this.close} appearance="subtle">
            Cancelar
            </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

class NewScore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      show: false,
      score: {},
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }
  close() {
    this.setState({ show: false });
  }
  open(rota) {
    this.setState({ rota: rota, show: true });
  }
  salvarScore() {
    let score = this.state.score;
    score.idUsuario = 1;
    score.idRota = this.state.rota.id;
    console.log(score);
    this.setState({ loading: true });
    fetch('/api/score/', {
      method: 'POST',
      body: JSON.stringify(score),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      this.props.refreshTable();
      this.close();
      this.setState({ loading: false });
    }).catch(err => { alert('Não foi possível salvar'); console.log(err); });
  }
  handlerDataChange(data) {
    this.state.score.data = data;
  }
  handlerValorChange(valor) {
    this.state.score.pontos = valor;
  }
  handlerDescricaoChange(descri) {
    this.state.score.descricao = descri;
  }
  render() {
    return (
      <Modal show={this.state.show} onHide={this.close} style={{ marginTop: '100px' }}>
        <Modal.Header>
          <Modal.Title> Nova coleta </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Data</p>
          <DatePicker format="DD/MM/YYYY" ranges={[]} onChange={this.handlerDataChange.bind(this)} />
          <p>Valor</p>
          <InputNumber onChange={this.handlerValorChange.bind(this)} />
          <p>Descrição</p>
          <Input onChange={this.handlerDescricaoChange.bind(this)} componentClass="textarea" rows={3} placeholder="Textarea" />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.salvarScore.bind(this)} appearance="primary" loading={this.state.loading}>
            Salvar
            </Button>
          <Button onClick={this.close} appearance="subtle">
            Cancelar
            </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}