import React, { Component } from 'react';
import { Panel, FlexboxGrid, Loader, InputGroup, Input, Icon } from 'rsuite';

import noImage from '../img/no_image.jpg';

const searchContainer = {
  padding: "20px 10px 10px 30px",
  maxWidth: "300px"
};

export default class Informations extends Component {  

  constructor() {
    super();
    this.state = {
      materialsOriginal: [],
      materials: [],
      isLoading: true,      
    };
  }  

  componentDidMount() {
    fetch('/api/materials')
      .then(response => {
        return response.json();
      })
      .then(list => {
        this.setState({ materials: list, isLoading: false });
      });
  }

  handleInputBusca(texto) {    
    if (this.state.materialsOriginal.length === 0) {
      this.setState({materialsOriginal: this.state.materials});
    }    
    if (texto === '') {
      this.setState({materials: this.state.materialsOriginal});
    } else {
      let filtered = this.state.materialsOriginal.filter(m => 
        m.nome.toLowerCase().indexOf(texto.toLowerCase()) > -1
        || m.nomeTipoMaterial.toLowerCase().indexOf(texto.toLowerCase()) > -1
      );      
      this.setState({materials: filtered});
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <div className="loading-page-container">
          <Loader size="lg" content="Carregando..." />
        </div>
      )
    }

    return (
      <div>
        <div style={searchContainer}>
          <InputGroup>
            <Input placeholder="Buscar..." onChange={this.handleInputBusca.bind(this)}/>
            <InputGroup.Addon>
              <Icon icon="search" />
            </InputGroup.Addon>
          </InputGroup>
        </div>
        <FlexboxGrid justify="space-around" className="cards-materials">
          {this.state.materials.map(m =>
            <MaterialCardItem key={m.id} nome={m.nome} tipo={m.nomeTipoMaterial} reciclavel={m.reciclavel} />
          )}
        </FlexboxGrid>
      </div>
    );
  }
}

class MaterialCardItem extends Component {

  getFooterClass() {
    return this.props.reciclavel ? 'reciclavel' : 'nao-reciclavel';
  }

  getFooterDesc() {
    return this.props.reciclavel ? 'Reciclável' : 'Não reciclável';
  }

  getImage() {    
    if (this.props.image === undefined) {      
      return noImage;
    }
    if (this.props.image === null) {      
      return noImage;
    }
    if (this.props.image === '') {      
      return noImage;
    }    
    return this.props.image;
  }

  render() {
    return (
      <FlexboxGrid.Item>
        <Panel bordered>
          <img src={this.getImage()} height="200px" alt="Material"/>
          <div className="title"> {this.props.nome} </div>
          <div> {this.props.tipo} </div>
          <div className={`footer ${this.getFooterClass()}`}> {this.getFooterDesc()} </div>
        </Panel>
      </FlexboxGrid.Item>
    )
  }
}