import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import { Progress, SelectPicker, Grid, Row, Col, Loader, PanelGroup, Panel, Timeline, Message, Table } from 'rsuite';
const { Column, HeaderCell, Cell } = Table;

const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={9}
    defaultCenter={{ lat: -28.679696, lng: -49.367481 }}
  >
    <Marker position={{ lat: -28.812901, lng: -49.201566 }} />
    <Marker position={{ lat: -28.679696, lng: -49.367481 }} />
    <Marker position={{ lat: -28.747888, lng: -49.474461 }} />
  </GoogleMap>
));

const styleContainerCharts = {
  display: 'flex',
  justifyContent: 'space-around',
  padding: '30px 0px',
}

const chart = {
  width: '120px',
}

const styleChartsDescription = {
  textAlign: 'center',
}

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      estados: [],
      cidades: [],
    }
  }

  componentDidMount() {
    fetch('/api/estados')
      .then(response => {
        return response.json();
      })
      .then(list => {
        this.setState({ estados: list.map(e => ({ value: e.id, label: e.nome })), isLoading: false });
      });
    fetch('/api/cidades')
      .then(response => {
        return response.json();
      })
      .then(list => {
        this.setState({ cidades: list.map(e => ({ value: e.id, label: e.nome, estado: e.id_estado })), isLoading: false });
      });
  }

  estadoHandler(e) {
    this.setState({ idEstado: e });
  }

  cidadeHandler(c) {
    this.setState({ idCidade: c });
  }

  render() {
    return (
      <div>
        <h2> Veja como estão as cidades participantes </h2>
        <MyMapComponent
          isMarkerShown
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOB58aVqe-CCROpo55DmdLV2sshgvUZ1c&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
        <div style={styleContainerCharts}>
          <div style={chart}>
            <Progress.Circle percent={20} />
            <div style={styleChartsDescription}> <strong> 20% </strong> estão fazendo coleta seletiva </div>
          </div>
          <div style={chart}>
            <Progress.Circle percent={50} />
            <div style={styleChartsDescription}> <strong> 50% </strong> estão utilizando aterros sanitários </div>
          </div>
        </div>
        <h2 className="green"> Encontre a sua cidade e acompanhe mais de perto </h2>
        <div style={{ padding: '0px 30px' }}>
          <Grid fluid>
            <Row className="show-grid">
              <Col xs={24} sm={12}>
                <SelectPicker placeholder="Estado" data={this.state.estados} block placement="topLeft" onChange={this.estadoHandler.bind(this)} />
              </Col>
              <Col xs={24} sm={12}>
                <SelectPicker placeholder="Cidade" data={this.state.cidades.filter(c => this.state.idEstado === undefined || c.estado === this.state.idEstado)} block placement="topRight" onChange={this.cidadeHandler.bind(this)} />
              </Col>
            </Row>
          </Grid>
        </div>
        <div style={{ padding: '30px' }}>
          {this.state.idCidade === undefined ? <div /> : <InfoCidade cidade={this.state.idCidade} estado={this.state.idEstado} />}
        </div>
      </div>
    );
  }
}

class InfoCidade extends Component {

  constructor() {
    super();
    this.state = {
      isLoading: true,
    }
  }

  componentDidMount() {
    this.fetchInitialData(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.estado === this.props.estado && nextProps.cidade === this.props.cidade) {
      return;
    }
    this.fetchInitialData(nextProps);
  }

  fetchInitialData(values) {
    this.setState({ cidade: undefined, bairros: undefined });
    fetch(`/api/locais/${values.estado}/${values.cidade}`)
      .then(response => {
        return response.json();
      })
      .then(list => {
        this.setState({ bairros: list });
      });
    fetch('/api/cidades')
      .then(response => {
        return response.json();
      })
      .then(list => {
        this.setState({ cidade: list.find(c => c.id === values.cidade) });
      });
  }

  render() {
    if (this.state.cidade === undefined || this.state.bairros === undefined) {
      return (
        <div className="loading-page-container">
          <Loader size="lg" content="Carregando..." />
        </div>
      )
    }
    return (
      <div>
        <div style={{fontSize: '30px', fontWeight: 'bold'}}> {this.state.cidade.nome}, </div>
        <div style={{marginBottom: '10px', paddingLeft: '2px'}}> SC </div>
        <Message
          showIcon
          type="warning"
          title="Esta região não informa dados sobre coleta seletiva :("
          description="Você deve pressionar os responsáveis por mais transparência nas informações."
        />        
        <PanelGroup accordion bordered>
          {this.state.bairros.map(b =>
            <InfoBairro key={b.id} bairro={b} />
          )}
        </PanelGroup>
      </div>
    );
  }
}

class InfoBairro extends Component {
  constructor() {
    super();
    this.state = {
      score: undefined,
      rotas: undefined,
    }
  }

  componentDidMount() {
    fetch('/api/score/' + this.props.bairro.id)
      .then(response => {
        return response.json();
      })
      .then(list => {
        this.setState({ score: list });
      });
    fetch('/api/rotas/bairro/' + this.props.bairro.id)
      .then(response => {
        return response.json();
      })
      .then(list => {
        list.map(l => {
          l.periodo = this.nomePeriodo(l.periodo);
          l.dia_semana = this.nomeDiaSemana(l.dia_semana);
          return l;
        })
        this.setState({ rotas: list });
      });
  }

  nomeDiaSemana(num) {
    switch (num) {      
      case 1: return 'Sábado';
      case 2: return 'Segunda';
      case 3: return 'Terça';
      case 4: return 'Quarta';
      case 5: return 'Quinta';
      case 6: return 'Sexta';      
      default: return 'Erro';
    }
  }

  nomePeriodo(letra) {
    if (letra === 'V') {
      return 'Vespertino';
    }
    return 'Matutino';
  }

  render() {
    let table, timeline;
    if (this.state.score === undefined || this.state.rotas === undefined) {
      return (
        <div className="loading-page-container">
          <Loader size="lg" content="Carregando..." />
        </div>
      )
    }
    if (this.state.rotas.length > 0) {
      table = (
        <div>
          <div> Horários de coleta </div>
          <Table data={this.state.rotas} autoHeight>
            <Column align="center" fixed>
              <HeaderCell>Dia da semana</HeaderCell>
              <Cell dataKey="dia_semana" />
            </Column>
            <Column align="center" fixed>
              <HeaderCell>Período</HeaderCell>
              <Cell dataKey="periodo" />
            </Column>
          </Table>
        </div>
      );
    }
    if (this.state.score.length > 0) {
      timeline = (
        <Timeline>
          {this.state.score.map(s =>
            <Timeline.Item key={s.id}>
              <div> {(new Date(s.data)).toLocaleDateString() + ' ' + s.descricao} </div>
              <div style={{ maxWidth: '600px' }}> <Progress.Line percent={s.valor/10} strokeColor={'success'} /> </div>
            </Timeline.Item>
          )}
        </Timeline>
      );
    } else {
      timeline = (
        <Message
          showIcon
          type="info"
          title="Sem informações"
          description="Ainda não foi adicionada nenhuma informação."/>
      );
    }
    return (
      <Panel key={this.props.bairro.id} header={this.props.bairro.nome}>
        {timeline}
        {table}
      </Panel>
    );
  }
}