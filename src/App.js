import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { IconButton, Icon, Navbar, Nav } from 'rsuite';

import 'rsuite/dist/styles/rsuite.min.css';
import './App.css';

import Home from './components/Home';
import Informations from './components/Informations';
import About from './components/About';
import UserArea from './components/User';

const NavLink = props => <Nav.Item componentClass={Link} {...props} />;

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      className: 'transparent'
    }
  }

  handleScroll() {
    //if (document.documentElement.scrollTop < (window.innerHeight * 0.9) - 56) {      
    if (document.documentElement.scrollTop < 100) {
      this.setState({
        className: 'transparent'
      });
    } else {
      this.setState({
        className: ''
      });
    }
  }

  componentDidMount() {
    window.onscroll = () => this.handleScroll();
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Navbar className={this.state.className}>
            <Navbar.Header>
              <Nav>
                <Nav.Item>ecolet</Nav.Item>
              </Nav>
            </Navbar.Header>
            <Navbar.Body>
              <Nav>
                <NavLink to="/">Início</NavLink>
                <NavLink to="/informations">Materiais</NavLink>
                <NavLink to="/about">Sobre o projeto</NavLink>
                <NavLink to="/">Notícias</NavLink>
                <NavLink to="/user">Usuário</NavLink>
              </Nav>
            </Navbar.Body>
          </Navbar>
          <header>
            <div className="content-info">
              <div>
                <h1>
                  <span className="label-small">Juntos pela </span>
                  <br />
                  <span className="label-big">coleta seletiva</span>
                </h1>
                <div className="descricao">
                  <p>
                    O Ecolet se enquadra na categoria de empreendedorismo social, que visa o desenvolvimento sustentável social, econômico e comunitário, atuando em uma necessidade local, na busca de uma melhor qualidade de vida. A ideia é aumentar a articulação do grupo produtivo e estimular a participação da população na esfera ambiental.
                  </p>
                </div>
                <div className="more">
                  <IconButton icon={<Icon icon="thumbs-up" />} color="green">Faça parte!</IconButton>
                </div>
              </div>
            </div>
            <div className="division-1" />
          </header>
          <main>
            <Route exact path="/" component={Home} />
            <Route exact path="/informations" component={Informations} />            
            <Route exact path="/about" component={About} />
            <Route exact path="/user" component={UserArea} />
          </main>
        </Router>
      </div>
    );
  }
}
